const path = require('path');

module.exports = () => {
    const packagepath = path.resolve('prepos.config.js');
    return require(packagepath);
}
