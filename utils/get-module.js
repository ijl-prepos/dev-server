const path = require('path');

const getModeulePath = (libName, libFilePath) => path.resolve('node_modules', libName, libFilePath)

module.exports = (req, res) => {
    const libName = req.params[0];
    const libVersion = req.params[1];
    const libFilePath = req.params[2];
    let filePath;

    if (libName === 'fire.app') {
        filePath = getModeulePath('@prepos/fire.app', `dist/${libFilePath}`);
    } else {
        filePath = getModeulePath(libName, libFilePath);
    }

    res.sendFile(filePath);
}
