const express = require('express');
const app = express();
const path = require('path');
const webpack = require('webpack');
const webpackDevMidleWare = require('webpack-dev-middleware');
const cookieSession = require('cookie-session');
const applyHbs = require('@prepos/templates');

const getModuleData = require('./utils/module-package');
const getModuleConfig = require('./utils/get-module-config');

const baseUrl = '/static';

const startServer = ({ port }) => {
    const moduleData = getModuleData();
    const appPath = `/${moduleData.cleanName}`;
    app.use(express.json({ extended: true }));
    app.use(cookieSession({
        name: 'session',
        keys: ['dasdadasdsasdas'],
    }));
    applyHbs(app);

    const config = require('@prepos/webpack-config');
    config.output.publicPath = `${baseUrl}/${moduleData.cleanName}/1.0.0/`;
    config.entry = moduleData.entryPoint;
    const compiler = webpack(config)
    // {
    //     mode: 'development',
    //     entry: moduleData.entryPoint,
    //     output: {
    //         filename: 'index.js',
    //         path: path.resolve('dist'),
    //         libraryTarget: 'umd',
    //         publicPath: `${baseUrl}/${moduleData.cleanName}/1.0.0/`
    //     },
    //     resolve: {
    //         extensions: ['.tsx', '.js', '.jsx', '.ts', '.json']
    //     },
    //     module: {
    //         rules: [{
    //             test: /\.tsx?$/,
    //             loader: 'ts-loader'
    //         }]
    //     }
    // });

    app.use(webpackDevMidleWare(compiler, {
        publicPath: config.output.publicPath,
    }));

    const {
        apps = {},
        navigation = {},
        config: appConfig = {},
        features = {},
    } = getModuleConfig();

    app.get(appPath, (req, res) => {
        res.render('index', {
            baseUrl: baseUrl,
            fireAppVersion: '1.0.0',
            title: 'My app',
            apps: { ...apps, [moduleData.cleanName]: { version: '1.0.0' } },
            navigations: { ...navigation, [moduleData.cleanName]: appPath, 'dummy.login': '/dummy/login' },
            config: { baseUrl: baseUrl, ...appConfig },
            features: { ...features }
        });
    });

    app.use(
        baseUrl,
        express.Router().get(/\/([.-\w]+)\/([.-\w\d]+)\/(.*)/, require('./utils/get-module'))
    );

    app.listen(port, () => {
        console.log(`🚀🚀🚀 server start on http://localhost:${port}${appPath}`);
    });
}

module.exports = startServer;
